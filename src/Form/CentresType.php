<?php

namespace App\Form;

use App\Entity\Centres;
use App\Form\CityAutocompleteField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CentresType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('adresse')
            ->add('adresseComplementaire')
            ->add('mail')
            ->add('telephone')
            ->add('twitter')
            ->add('facebook')
            ->add('instagram')
            ->add('localite', CityAutocompleteField::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Centres::class,
        ]);
    }
}
