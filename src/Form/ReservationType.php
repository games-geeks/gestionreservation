<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Niveaux;
use App\Entity\Reservation;
use Doctrine\ORM\QueryBuilder;
use App\Entity\ModeleReservation;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('jour', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('heureDebut', null, [
                'widget' => 'single_text',
            ])
            ->add('heureFin', null, [
                'widget' => 'single_text',
            ])
            ->add('nombreMax')
            ->add('semaine')

            ->add('niveau', EntityType::class, [
                'class' => Niveaux::class,
                'choice_label' => 'nom',
                'multiple' => true,
            ])
            ->add('coach', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->setParameter('role', '%"' . 'ROLE_COACH' . '"%')
                        ->orderBy('u.prenom', 'ASC');
                },
                'choice_label' => 'prenom',
            ])
            ->add('isActive')
            ->add('save', SubmitType::class, [
                'label' => 'Envoyer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
