<?php

namespace App\Form;

use App\Entity\AbonnementsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AbonnementsTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            ->add('description')
            ->add('tarif')
            ->add('nombreLecon')
            ->add('save', SubmitType::class, [
                'label' => 'Envoyer'
            ]);
        // ->add('centre');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AbonnementsType::class,
        ]);
    }
}
