<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Niveaux;
use App\Entity\Reservation;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserType extends AbstractType
{
    public function __construct(private FormListenerFactory $flf)
    {
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class)
            // ->add('roles')
            ->add('nom')
            ->add('prenom')
            ->add('telephone')
            ->add('nombreLeconsRestantes')
            ->add('dateNaissance', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('niveau', EntityType::class, [
                'class' => Niveaux::class,
                'choice_label' => 'nom',
            ])
            ->add('isVerified')
            ->add('save', SubmitType::class, [
                'label' => 'Envoyer'
            ])
            ->addEventListener(FormEvents::POST_SUBMIT, $this->flf->autoMDP())
            ->addEventListener(FormEvents::POST_SUBMIT, $this->flf->rolePlayer());
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
