<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Niveaux;
use Doctrine\ORM\QueryBuilder;
use App\Entity\ModeleReservation;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ModeleReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('jour')
            ->add('heureDebut', TimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('heureFin', TimeType::class, [
                'widget' => 'single_text',
            ])
            ->add('nombreMax')
            ->add('niveaux', EntityType::class, [
                'class' => Niveaux::class,
                'choice_label' => 'nom',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('coach', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->setParameter('role', '%"' . 'ROLE_COACH' . '"%')
                        ->orderBy('u.prenom', 'ASC');
                },
                'choice_label' => 'prenom',
            ])
            ->add('isActive')
            ->add('save', SubmitType::class, [
                'label' => 'Envoyer'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ModeleReservation::class,
        ]);
    }
}
