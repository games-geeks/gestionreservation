<?php

namespace App\DataFixtures;

use App\Entity\Statut;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class StatutFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $statut = new Statut;
        $statut->setNom('Réservé');
        $statut->setCouleur('warning');

        $manager->persist($statut);

        $statut = new Statut;
        $statut->setNom('Annulé');
        $statut->setCouleur('danger');
        $manager->persist($statut);

        $statut = new Statut;
        $statut->setNom('Confirmé');
        $statut->setCouleur('success');
        $manager->persist($statut);

        $manager->flush();
    }
}
