<?php

namespace App\DataFixtures;

use App\Entity\Coach;
use App\Entity\Joueur;
use Faker;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UsersFixtures extends Fixture implements DependentFixtureInterface
{
    public function __construct(
        private UserPasswordHasherInterface $passwordEncoder,
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $city = $this->getReference('city-' . rand(1, 150));
        $admin = new Joueur();
        $admin->setEmail('denny@games-geeks.fr');
        $admin->setNom('Scott');
        $admin->setPrenom('Denny');
        // $admin->setAdresse('12 rue du port');
        // $admin->setAdresseComplementaire('06800');
        $admin->setTelephone('0612485792');
        // $admin->setLocalite($city);
        $admin->setIsVerified(true);
        $admin->setPassword(
            $this->passwordEncoder->hashPassword($admin, 'admin')
        );
        $admin->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        $faker = Faker\Factory::create('fr_FR');

        for ($usr = 1; $usr <= 20; $usr++) {
            $user = new Joueur();
            $user->setEmail($faker->email);
            $user->setNiveau($this->getReference(('niv-' . rand(1, 4))));
            $user->setnom($faker->lastName);
            $user->setPrenom($faker->firstName);
            // $user->setAdresse($faker->streetAddress);
            // $user->setAdresseComplementaire($faker->buildingNumber);
            $user->setTelephone($faker->phoneNumber());
            $user->setDateNaissance($faker->dateTimeThisCentury());
            $city = $this->getReference('city-' . rand(1, 150));
            // $user->setLocalite($city);
            $user->setPassword(
                $this->passwordEncoder->hashPassword($user, 'secret')
            );
            $user->setRoles(['ROLE_JOUEUR']);
            $manager->persist($user);
        }
        for ($usr = 1; $usr <= 5; $usr++) {
            $user = new Coach();
            $prenom = $faker->firstName;
            $user->setEmail($faker->email);
            $user->setnom($faker->lastName);
            $user->setPrenom($prenom);
            $user->setSurnom($prenom);
            // $user->setAdresse($faker->streetAddress);
            // $user->setAdresseComplementaire($faker->buildingNumber);
            $user->setTelephone($faker->phoneNumber());
            $city = $this->getReference('city-' . rand(1, 150));
            // $user->setLocalite($city);
            $user->setPassword(
                $this->passwordEncoder->hashPassword($user, 'secret')
            );
            $user->setRoles(['ROLE_COACH']);
            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            NiveauFixtures::class
        ];
    }
}
