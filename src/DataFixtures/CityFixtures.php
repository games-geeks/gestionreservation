<?php

namespace App\DataFixtures;

use App\Entity\City;
use League\Csv\Reader;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;


class CityFixtures extends Fixture
{
    private $counter = 1;

    public function load(ObjectManager $manager): void
    {

        $cities = $this->readCsvFile();
        foreach ($cities as $arrayCity) {
            $city  = $this->createOrUpdateCity($arrayCity);
            $manager->persist($city);
        }

        $manager->flush();
    }

    private function readCsvFile(): Reader
    {

        $csv = Reader::createFromPath('%kernel.root_dir%/../import/cities_ex.csv', 'r');
        $csv->setHeaderOffset(0);
        return $csv;
    }

    private function createOrUpdateCity(array $arrayCity): City
    {
        $city =  new City;
        $city->setInseeCode($arrayCity['insee_code']);
        $city->setCityCode($arrayCity['city_code']);
        $city->setZipCode($arrayCity['zip_code']);
        $city->setLabel($arrayCity['label']);
        $city->setLatitude($arrayCity['latitude']);
        $city->setLongitude($arrayCity['longitude']);
        $city->setDepartmentName($arrayCity['department_name']);
        $city->setDepartmentNumber($arrayCity['department_number']);
        $city->setRegionName($arrayCity['region_name']);
        $city->setRegionGeoJsonName($arrayCity['region_geojson_name']);
        $this->addReference('city-' . $this->counter, $city);
        $this->counter++;

        return $city;
    }
}
