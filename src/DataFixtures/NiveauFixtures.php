<?php

namespace App\DataFixtures;

use App\Entity\Niveaux;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;


class NiveauFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {

        $niveau = new Niveaux;
        $niveau->setNom('1 étoile');
        $niveau->setDescription('Cours pour les débutants');
        $this->addReference('niv-1', $niveau);

        $manager->persist($niveau);

        $niveau = new Niveaux;
        $niveau->setNom('2 étoiles');
        $niveau->setDescription('Cours les personnes ayant pratiqué le tennis');
        $manager->persist($niveau);
        $this->addReference('niv-2', $niveau);

        $niveau = new Niveaux;
        $niveau->setNom('3 étoiles');
        $niveau->setDescription('Personnes confirmées non classées');
        $manager->persist($niveau);
        $this->addReference('niv-3', $niveau);

        $niveau = new Niveaux;
        $niveau->setNom('4 étoiles');
        $niveau->setDescription('Personnes clasées 40 à 30-2');
        $manager->persist($niveau);
        $this->addReference('niv-4', $niveau);
        $manager->flush();
    }
}
