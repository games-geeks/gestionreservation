<?php

namespace App\DataFixtures;

use App\Entity\Centres;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class CentreFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $centre = new Centres;
        $centre->setNom('US Cagnes');
        $centre->setMail('a@a.com');
        $centre->setTelephone('0492343235');
        $centre->setAdresse('Paul Sauveigo');
        $this->addReference('cen-1', $centre);
        $city = $this->getReference('city-' . rand(1, 150));
        $centre->setLocalite($city);
        $manager->persist($centre);


        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            CityFixtures::class
        ];
    }
}
