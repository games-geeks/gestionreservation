<?php

namespace App\DataFixtures;

use App\Entity\AbonnementsType;
use App\DataFixtures\CentreFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class AbonnementFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {

        //On va chercher une référence de centre
        $centre = $this->getReference('cen-1');


        //         Packakge 10 leçons;10;Package contenant 10 leçons;18000;1
        // Packakge 30 leçons;30;Package contenant 30 leçons;40000;1
        // Packakge 60 leçons;60;Package contenant 60 leçons;60000;1
        $abonnement = new AbonnementsType;
        $abonnement->setNom('Packakge 10 leçons');
        $abonnement->setDescription('Package contenant 10 leçons');
        $abonnement->setTarif(180);
        $abonnement->setNombreLecon(10);
        $manager->persist($abonnement);

        $abonnement = new AbonnementsType;
        $abonnement->setNom('Packakge 30 leçons');
        $abonnement->setDescription('Package contenant 30 leçons');
        $abonnement->setTarif(400);
        $abonnement->setNombreLecon(30);
        $manager->persist($abonnement);

        $abonnement = new AbonnementsType;
        $abonnement->setNom('Packakge 60 leçons');
        $abonnement->setDescription('Package contenant 60 leçons');
        $abonnement->setTarif(180);
        $abonnement->setNombreLecon(10);
        $manager->persist($abonnement);

        $manager->flush();
    }


    public function getDependencies(): array
    {
        return [
            CentreFixtures::class
        ];
    }
}
