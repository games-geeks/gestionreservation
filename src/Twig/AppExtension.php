<?php

namespace App\Twig;

use DateTime;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;


class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
            new TwigFilter('getAge', [$this, 'getAge']),
            new TwigFilter('getDay', [$this, 'getDay']),
        ];
    }


    public function getFunctions(): array
    {
        return [
            new TwigFunction('pluralize', [$this, 'pluralize']),


        ];
    }

    /**
     * @param int count le nombre à controler
     * @param string singular le singuler
     * @param plural optionnel
     *
     * @return string
     */
    public function pluralize(int $count, string $singular, ?string $plural = null): string
    {
        /* Si pas de pluriel, on le contruit*/
        $plural ??=  $singular . 's';
        /* On regarde selon le nombre ce que l'on doit retourner*/
        $res = $count > 1 ? $plural : $singular;

        return $count . ' ' . $res;
    }

    public function getAge(DateTime $date): int
    {
        $now = new \DateTime('now');

        $diff = $now->diff($date);

        //return $difference->format('%y years, %m months, %d days old.');
        return $diff->y;
    }
    public function getDay(int $day): string
    {
        $english_days = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
        $french_days = array('Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');
        return str_replace($english_days, $french_days, date('l', mktime(0, 0, 0, 1, $day, 1970)));
        //    return date('l', mktime(0, 0, 0, 1, $day, 1970));
    }
}
