<?php

namespace App\Repository;

use DateTime;
use App\Entity\User;
use DateTimeInterface;
use App\Entity\Reservation;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Reservation>
 *
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private PaginatorInterface $paginator)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function save(Reservation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Reservation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findCoursForDay(string $jour): ?array
    {
        // dd($jour);
        return $this->createQueryBuilder('r')
            ->andWhere('r.jour = :jour')
            ->setParameter('jour', $jour)
            ->getQuery()
            ->getResult();
    }
    public function findCours(string $heureDebut, string $heureFin, string $jour, User $coach): ?Reservation
    {
        // dd($heureDebut, $heureFin, $jour, $coach);
        return $this->createQueryBuilder('r')
            ->andWhere('r.jour = :jour')
            ->setParameter('jour', $jour)
            ->andWhere('r.heureDebut = :heureD')
            ->setParameter('heureD', $heureDebut)
            ->andWhere('r.heureFin = :heureF')
            ->setParameter('heureF', $heureFin)
            ->andWhere('r.coach = :val')
            ->setParameter('val', $coach)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function findResa(Reservation $resa): ?Reservation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.jour = :val')
            ->setParameter('val', $resa->getJour())
            ->andWhere('r.heureDebut = :val')
            ->setParameter('val', $resa->getHeureDebut())
            ->andWhere('r.heureFin = :val')
            ->setParameter('val', $resa->getHeureFin())
            ->andWhere('r.coach = :val')
            ->setParameter('val', $resa->getCoach())
            ->andWhere('r.isModele = false')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function paginateCours(int $page): PaginationInterface
    {
        $builder = $this->createQueryBuilder('r');
        return $this->paginator->paginate(
            $builder,
            $page,
            40,
            [
                'distinct' => false,
                'sortFieldAllowList' => ['r.id']
            ]
        );
    }

    //    /**
    //     * @return Reservation[] Returns an array of Reservation objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('r.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Reservation
    //    {
    //        return $this->createQueryBuilder('r')
    //            ->andWhere('r.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
