<?php

namespace App\Repository;

use App\Entity\ModeleReservation;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<ModeleReservation>
 *
 * @method ModeleReservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModeleReservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModeleReservation[]    findAll()
 * @method ModeleReservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModeleReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private PaginatorInterface $paginator)
    {
        parent::__construct($registry, ModeleReservation::class);
    }

    public function paginateModele(int $page): PaginationInterface
    {
        $builder = $this->createQueryBuilder('r');
        return $this->paginator->paginate(
            $builder,
            $page,
            20,
            [
                'distinct' => false,
                'sortFieldAllowList' => ['r.id']
            ]
        );
    }
    public function getCoursActive(): array
    {
        return  $this->createQueryBuilder('r')
            ->andWhere('r.isActive  =1')
            ->getQuery()
            ->getResult();
    }

    //    /**
    //     * @return ModeleReservation[] Returns an array of ModeleReservation objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('m.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?ModeleReservation
    //    {
    //        return $this->createQueryBuilder('m')
    //            ->andWhere('m.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
