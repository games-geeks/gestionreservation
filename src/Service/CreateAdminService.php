<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Joueur;
use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $userPasswordHasher
    ) {
    }

    public function create(string $email, string $password): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        // On crée user si il n'existe pas
        if (!$user) {
            $user = new Joueur();
            $user->setEmail($email);
            $user->setNom('admin');
            $user->setPrenom('admin');
            $user->setIsVerified(1);
            $user->setTelephone('0000000000');
            $password = $this->userPasswordHasher->hashPassword($user, $password);
            $user->setPassword($password);
        }
        // Dans tous les cas, on lui donne les droits admin
        $user->setRoles(['ROLE_ADMIN', 'ROLE_JOUEUR']);

        $this->userRepository->save($user, true);
    }
}
