<?php

namespace App\Service;

use DateTime;
use DateInterval;


class DateService
{
    public function __construct()
    {
    }

    public static function getAllDaysOfYear(int $annee, int $jour): array
    {
        $days = array();
        if (date("Y") >= $annee)
            $year = date($annee); // Récupérer l'année en cours
        if ($jour >= 1 && $jour <= 7) {


            // Boucle sur les 12 mois de l'année
            for ($month = 1; $month <= 12; $month++) {
                $date = new DateTime("$year-$month-01"); // Créer une instance de la classe DateTime pour le 1er jour du mois
                $weekDay = $date->format("N"); // Récupérer le numéro du jour de la semaine pour ce jour
                //1 - lundi, 2 - Mardi etc...
                $daysToAdd = $jour - $weekDay; // Calculer le nombre de jours à ajouter pour arriver au premier lundi du mois
                if ($daysToAdd < 0) {
                    $daysToAdd += 7; // Si le premier jour du mois n'est pas un lundi, ajouter des jours supplémentaires pour atteindre le prochain lundi
                }
                $date->add(new DateInterval("P{$daysToAdd}D")); // Ajouter les jours nécessaires pour arriver au premier lundi du mois
                while ($date->format("Y") == $year && $date->format("n") == $month) { // Tant que le mois en cours est l'année en cours
                    $days[] = $date->format("Y-m-d"); // Ajouter la date du lundi à la liste
                    $date->add(new DateInterval("P7D")); // Passer au lundi suivant
                }
            }

            // Afficher les lundis de l'année en cours
            // echo "Les lundis de $year : ";
            // foreach ($days as $monday) {
            //     echo "$monday, ";
            // }
            // dd($days);
        }
        return $days;
    }
}
