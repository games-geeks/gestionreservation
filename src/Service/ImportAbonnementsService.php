<?php

namespace App\Service;

use App\Entity\AbonnementsType;
use App\Repository\CentresRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportAbonnementsService
{
    public function __construct(private EntityManagerInterface $em, private CentresRepository $centresRepository)
    {
    }
    public function importAbonnements(SymfonyStyle $io): void
    {
        $io->title('Importation des abonnements');
        $abos = $this->readCsvFile();
        $io->progressStart(count($abos));

        foreach ($abos as $arrayabonnement) {
            $io->progressAdvance();
            $abonnement  = $this->createOrUpdateabonnement($arrayabonnement);
            $this->em->persist($abonnement);
        }
        $this->em->flush();
        $io->progressFinish();
        $io->success('Importation terminée');
    }

    private function readCsvFile(): Reader
    {

        $csv = Reader::createFromPath('%kernel.root_dir%/../import/abonnements.csv', 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        return $csv;
    }

    private function createOrUpdateAbonnement(array $arrayAbonnement): AbonnementsType
    {

        $abonnement = new AbonnementsType;
        $abonnement->setNom($arrayAbonnement['nom']);
        $abonnement->setNombreLecon($arrayAbonnement['nombre_lecon']);
        $abonnement->setDescription($arrayAbonnement['description']);
        $abonnement->setTarif($arrayAbonnement['tarif']);
        // $abonnement->setCentre($this->centresRepository->findOneBy(['id' => $arrayAbonnement['centre']]));


        return $abonnement;
    }
}
