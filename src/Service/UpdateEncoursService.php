<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Joueur;
use App\Entity\Reservation;
use App\Repository\JoueurRepository;
use App\Repository\ReservationRepository;
use App\Repository\StatutRepository;
use Doctrine\ORM\EntityManagerInterface;

class UpdateEncoursService
{
    public function __construct(
        private readonly StatutRepository $statutRepository,
        private readonly ReservationRepository $reservationRepository,
        private readonly EntityManagerInterface $manager
    ) {
    }

    /**
     * Fonction permettant de mettre à jour les encours pour une journée donnée
     * A faire de préférence à la fin de la journée
     * @TODO Voir si on peut améliorer la sélection, la fréquence, ou bien passé un date en paramètre
     */
    public  function updateEncours(): void
    {
        //récupérer toutes les reservations du jour
        $theDay = new \DateTime();
        // dd(date('Y/m/d'));
        $statut = $this->statutRepository->findOneBy(['id' => 3]);
        $resas = $this->reservationRepository->findCoursForDay($theDay->format('Y-m-d'));
        foreach ($resas as $resa) {
            //Pour chaque résa trouvée, on va lister les joueurs
            $joueurs = $resa->getJoueur();
            foreach ($joueurs as $joueur) {
                // dd($joueur);
                $joueur->setNombreLeconsRestantes($joueur->getNombreLeconsRestantes() - 1);
                //changer le statut
                $histos = $resa->getReservationHistoriques();
                foreach ($histos as $histo) {
                    $histo->setStatut($statut);
                }
            }
        }
        $this->manager->flush();
    }

    public function checkIfResaIsInPast(Reservation $reservation, Joueur $joueur, JoueurRepository $joueurRepository): void
    {
        //on recrédite le nombre de leçon restantes seulement si la date du cours est dans le passé

        $theDay = new \DateTime();
        // dd($theDay, $reservation->getJour());
        if ($reservation->getJour()->format(('Ymd')) < $theDay->format('Ymd')) {
            //dd($theDay->format('Ymd'));
            $joueur->setNombreLeconsRestantes($joueur->getNombreLeconsRestantes() + 1);
            $joueurRepository->save($joueur, true);
        }
    }
}
