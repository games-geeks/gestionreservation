<?php

namespace App\EventSubscriber;


use App\Repository\CentresRepository;
use App\Service\SendMailService;
use App\Repository\UserRepository;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class WorkflowSubscriber implements EventSubscriberInterface
{

    /**
     *La demande de validation de l'achat doit se faire via le controler
     * Quand la demande est faite, un mail doit être envoyé aux coaches
     * Les coachs demandent le paiement
     * Quand celui-ci est reçu, on augmente le nombre de leçon
     */

    public function __construct(
        private SendMailService $mail,
        private UrlGeneratorInterface $url,
        private UserRepository $userRepository,
        private MailerInterface $mailer,
        private CentresRepository $centresRepository
    ) {
    }


    public function newAchatCoachValidation(Event $event)
    {

        $email = (new TemplatedEmail())
            ->from('no-reply@gestionReservation.fr')
            ->subject('Nouvelle validation pour un achat')
            ->htmlTemplate('emails/workflow/nouvelle_demande_achat.html.twig');

        //récuperer les mails de tous les coach-plus
        $users = $this->userRepository->getEmailbyRoles('COACH');
        foreach ($users as $user) {
            $email->addTo($user->getEmail());
        }
        //on prépare le contexte du mail
        $url = $this->url->generate('achat_status', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $user = $event->getSubject()->getJoueur();
        $abonnement = $event->getSubject()->getAbonnement();
        $prix = $event->getSubject()->getPrixPaye();
        //dd($event);
        $context = compact('user', 'abonnement', 'prix', 'url');
        //on envoie un mail
        $email->context($context);
        $this->mailer->send($email);
    }

    /**
     * La demande est validée par le coach
     * On demande donc le paiement
     */
    public function newWaitPaiement(Event $event)
    {
        $email = (new TemplatedEmail())
            ->from('no-reply@gestionReservation.fr')
            ->subject('En attente de Paiement')
            ->htmlTemplate('emails/workflow/attente_paiement.html.twig');
        //on prépare le contexte du mail
        $url = $this->url->generate('achat_status', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $user = $event->getSubject()->getJoueur();
        $abonnement = $event->getSubject()->getAbonnement();
        $prix = $event->getSubject()->getPrixPaye();
        //dd($event);
        $context = compact('user', 'abonnement', 'prix', 'url');
        //on envoie un mail
        $email->to($user->getEmail());
        $email->context($context);
        $this->mailer->send($email);
    }

    /**
     * Le paiement est fait on demande aux coach de vérfier et de valider l'ajout des leçons
     */
    public function PaiementDone(Event $event)
    {
        $email = (new TemplatedEmail())
            ->from('no-reply@gestionReservation.fr')
            ->subject('Paiement Validé')
            ->htmlTemplate('emails/workflow/paiement_ok.html.twig');

        //récuperer les mails de tous les coach-plus
        $users = $this->userRepository->getEmailbyRoles('COACH');
        foreach ($users as $user) {
            $email->addTo($user->getEmail());
        }
        //on prépare le contexte du mail
        $url = $this->url->generate('achat_status', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $user = $event->getSubject()->getJoueur();
        $abonnement = $event->getSubject()->getAbonnement();
        $prix = $event->getSubject()->getPrixPaye();
        //dd($event);
        $context = compact('user', 'abonnement', 'prix', 'url');
        //on envoie un mail
        $email->context($context);
        $this->mailer->send($email);
    }

    /**
     * Tout est ok, on crédite les leçons.
     */
    public function PaiementOK(Event $event)
    {

        //on prépare le contexte du mail
        $url = $this->url->generate('achat_status', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $user = $event->getSubject()->getJoueur();
        $abonnement = $event->getSubject()->getAbonnement();
        $prix = $event->getSubject()->getPrixPaye();
        //dd($event);
        $context = compact('user', 'abonnement', 'prix', 'url');
        //on regarde si il y a un encours
        //// Récupération du centre (dans un éventuel cas où fait multicentre)
        $user->setNombreLeconsRestantes($user->getNombreLeconsRestantes()  + $abonnement->getNombreLecon());
        $this->userRepository->save($user, true);
        //on envoie un mail
        $email = (new TemplatedEmail())
            ->from('no-reply@gestionReservation.fr')
            ->subject('Tout est Ok')
            ->htmlTemplate('emails/workflow/achat_valide.html.twig')
            ->to($user->getEmail())
            ->context($context);
        $this->mailer->send($email);
    }

    public static function getSubscribedEvents()
    {
        return [

            'workflow.achat_request.entered.coach_validation_pending' => 'newAchatCoachValidation',
            'workflow.achat_request.leave.coach_validation_pending' => 'newWaitPaiement',
            'workflow.achat_request.leave.order_paiement' => 'PaiementDone',
            'workflow.achat_request.entered.order_ok' => 'PaiementOK',

        ];
    }
}
