<?php

namespace App\Controller\myAdmin;

use App\Entity\Coach;
use App\Entity\User;
use App\Form\CoachType;
use App\Form\UserType;
use App\Repository\CoachRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/administration/coach', name: 'myadmin.coach.')]
class CoachController extends AbstractController
{

    #[Route('/', name: 'list')]
    public function index(Request $request, CoachRepository $userRepository, Security $security): Response
    {
        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $coachs = $userRepository->paginateUser($page, 'COACH');
        return $this->render('myAdmin/coach/index.html.twig', compact('coachs'));
    }

    #[Route('/creation', name: 'create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $coach =  new Coach;
        $form = $this->createForm(CoachType::class, $coach);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $coach->setRoles(["ROLE_COACH"]);
            $em->persist($coach);
            $em->flush();
            $this->addFlash('success', 'Le coach a été créé');
            return $this->redirectToRoute('myadmin.coach.list');
        }
        return $this->render(
            'myAdmin/coach/add.html.twig',
            compact('form')
        );
    }

    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        Coach $user,
        UserRepository $userRepository
    ): Response {
        $form = $this->createForm(CoachType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Le coach a été modifié');
            return $this->redirectToRoute('myadmin.coach.list');
        }
        return $this->render(
            'myAdmin/coach/edit.html.twig',
            compact('user', 'form')
        );
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function delete(Coach $coach,  EntityManagerInterface $em)
    {
        $message = 'Le coach a été supprimé';
        $em->remove($coach);
        $em->flush();

        $this->addFlash('success', $message);
        return $this->redirectToRoute('myadmin.coach.list');
    }

    #[Route('/up/{id}', name: 'promote', requirements: ['id' => Requirement::DIGITS], methods: ['GET'])]
    // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function promote(Coach $coach,  EntityManagerInterface $em)
    {
        $coach->setRoles(['ROLE_SUPER_COACH']);
        $em->flush();

        $this->addFlash('success', 'Le coach a bien le nouveau role');
        return $this->redirectToRoute('myadmin.coach.list');
    }
    #[Route('/down/{id}', name: 'decrease', requirements: ['id' => Requirement::DIGITS], methods: ['GET'])]
    // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function decrease(Coach $coach,  EntityManagerInterface $em)
    {

        $coach->setRoles(['ROLE_COACH']);
        $em->flush();

        $this->addFlash('success', 'Les roles ont bien été modifiés');
        return $this->redirectToRoute('myadmin.coach.list');
    }
}
