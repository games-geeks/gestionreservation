<?php

namespace App\Controller\myAdmin;

use App\Entity\Reservation;
use App\Entity\User;
use App\Form\ReservationType;
use App\Form\UserType;
use App\Repository\ReservationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/administration/cours', name: 'myadmin.cours.')]
class CoursController extends AbstractController
{

    #[Route('/', name: 'list')]
    public function index(Request $request, ReservationRepository $coursRepo, Security $security): Response
    {
        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $cours = $coursRepo->paginateCours($page);
        return $this->render('myAdmin/cours/index.html.twig', compact('cours'));
    }

    #[Route('/creation', name: 'create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $resa =  new Reservation;
        $form = $this->createForm(ReservationType::class, $resa);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($resa);
            $em->flush();
            $this->addFlash('success', 'Le cours a été créé');
            return $this->redirectToRoute('myadmin.cours.list');
        }
        return $this->render(
            'myAdmin/cours/add.html.twig',
            compact('form')
        );
    }

    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        Reservation $cours,
        ReservationRepository $coursRepository
    ): Response {
        $form = $this->createForm(ReservationType::class, $cours);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Le cours a été modifié');
            return $this->redirectToRoute('myadmin.cours.list');
        }
        return $this->render(
            'myAdmin/cours/edit.html.twig',
            compact('cours', 'form')
        );
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function delete(Reservation $cours,  EntityManagerInterface $em)
    {
        if ($cours->getNombreEncours() > 0) {
            $cours->setIsActive(false);
            $message = 'Le cours a été masqué';
            //ajouter la gestion des encours pour les membres inscrits
        } else {
            $message = 'Le cours a été supprimé';
            $em->remove($cours);
        }
        $em->flush();

        $this->addFlash('success', $message);
        return $this->redirectToRoute('myadmin.cours.list');
    }
}
