<?php

namespace App\Controller\myAdmin;

use App\Entity\User;
use App\Entity\Joueur;
use App\Form\UserType;
use App\Form\JoueurType;
use App\Repository\UserRepository;
use App\Repository\JoueurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/administration/abonne', name: 'myadmin.abonne.')]
class AbonneController extends AbstractController
{

    #[Route('/', name: 'list')]
    public function index(Request $request, JoueurRepository $userRepository, Security $security): Response
    {
        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $abonnes = $userRepository->paginateUser($page, 'ROLE_JOUEUR');
        return $this->render('myAdmin/abonne/index.html.twig', compact('abonnes'));
    }

    #[Route('/creation', name: 'create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $abonne =  new Joueur;
        $form = $this->createForm(JoueurType::class, $abonne);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($abonne);
            $em->flush();
            $this->addFlash('success', 'L\'abonné a été créé');
            return $this->redirectToRoute('myadmin.abonne.list');
        }
        return $this->render(
            'myAdmin/abonne/add.html.twig',
            compact('form')
        );
    }

    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        Joueur $user,
        UserRepository $userRepository
    ): Response {
        $form = $this->createForm(JoueurType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'L\abonné a été modifié');
            return $this->redirectToRoute('myadmin.abonne.list');
        }
        return $this->render(
            'myAdmin/abonne/edit.html.twig',
            compact('user', 'form')
        );
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function delete(User $abonne,  EntityManagerInterface $em)
    {
        $message = 'L\'abonné a été supprimé';
        $em->remove($abonne);
        $em->flush();

        $this->addFlash('success', $message);
        return $this->redirectToRoute('myadmin.abonne.list');
    }
}
