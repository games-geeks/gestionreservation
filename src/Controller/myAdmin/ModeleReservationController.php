<?php

namespace App\Controller\myAdmin;

use App\Entity\Reservation;
use App\Service\DateService;
use App\Entity\ModeleReservation;
use App\Form\CreationCoursType;
use App\Form\ModeleReservationType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ReservationRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Repository\ModeleReservationRepository;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/administration/modeleReservation', name: 'myadmin.modele.reservation.')]
class ModeleReservationController extends AbstractController
{
    /**
     *  TODO : Rajouter les droits
     */
    #[Route('/', name: 'list')]
    public function index(Request $request, ModeleReservationRepository $modeleReservationRepository, Security $security): Response
    {
        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $modeleReservation = $modeleReservationRepository->paginateModele($page);
        return $this->render('myAdmin/modele_reservation/index.html.twig', compact('modeleReservation'));
    }

    #[Route('/creation', name: 'create')]
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $modele =  new ModeleReservation;
        $form = $this->createForm(ModeleReservationType::class, $modele);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($modele);
            $em->flush();
            $this->addFlash('success', 'Le modèle de cours a été créé');
            return $this->redirectToRoute('myadmin.modele.reservation.list');
        }
        return $this->render(
            'myAdmin/modele_reservation/add.html.twig',
            compact('form')
        );
    }

    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(
        Request $request,
        EntityManagerInterface $em,
        ModeleReservation $modele,
        ModeleReservationRepository $modeleReservationRepository
    ): Response {
        $form = $this->createForm(ModeleReservationType::class, $modele);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Le modèle de cours a été modifié');
            return $this->redirectToRoute('myadmin.modele.reservation.list');
        }
        return $this->render(
            'myAdmin/modele_reservation/edit.html.twig',
            compact('modele', 'form')
        );
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    // #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function delete(Request $request, ModeleReservation $modele,  EntityManagerInterface $em)
    {
        $message = 'Le modèle de cours a été supprimé';
        $em->remove($modele);
        $em->flush();

        $this->addFlash('success', $message);
        return $this->redirectToRoute('myadmin.modele.reservation.list');
    }

    #[Route('/generation/{id}', name: 'generate',  methods: ['GET', 'POST'])]
    public function generation(Request $request, ReservationRepository $resaR, ModeleReservation $resa): Response
    {
        //on récupère le jour de la semaine
        // $jour = $resa->getJour()->format('N');
        $jour = $resa->getJour();
        $annee = date('Y');
        //on récupère tous les jours de l'année correspondant au modèle
        $days = DateService::getAllDaysOfYear($annee, $jour);
        $i = 0;
        foreach ($days as $day) {
            if (date('m/d/y', strtotime($day)) > date("m/d/y")) { // on regarde que l'on génère une date future.

                //On génère une nouvelle résa
                $newResa = new Reservation;
                $newJour = new \DateTime($day);
                $semaine = date("W", strtotime($day));
                $newResa->setJour($newJour);
                $newResa->setSemaine($semaine);
                $newResa->setHeureDebut($resa->getHeureDebut());
                $newResa->setHeureFin($resa->getHeureFin());
                foreach ($resa->getNiveaux() as $niv)
                    $newResa->addNiveau($niv);
                $newResa->setNombreMax($resa->getNombreMax());
                $newResa->setNombreEncours(0);
                $newResa->setCoach($resa->getCoach());
                $newResa->setModele($resa);
                $newResa->setIsActive(true);
                // dd($newResa);
                //Controler que la date n'est pas déjà présente pour le meme coach à la même heure
                if (!$resaR->findResa($newResa)) {
                    //si ce n'est pas le cas, on génère une nouvelle résa
                    $resaR->save($newResa, true);
                    $i++;
                }
            }
        }
        // dd($i);
        return $this->redirectToRoute('main');
        // return $this->render('reservation/generation.html.twig', [
        //     'controller_name' => 'ReservationController',
        //     'resas' => $resaR->findBy(['isModele' => true])
        // ]);
    }

    #[Route('/creationCours', name: 'generateMasse',  methods: ['GET', 'POST'])]
    public function generationEnMasse(Request $request, ModeleReservationRepository $modeleReservationRepository): Response
    {

        $page = $request->query->getInt('page', 1);
        // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $modeleReservation = $modeleReservationRepository->paginateModele($page);
        return $this->render('myAdmin/modele_reservation/generation.html.twig', compact('modeleReservation'));
    }

    #[Route('/masse', name: 'masse',  methods: ['GET', 'POST'])]
    public function Masse(
        Request $request,
        ModeleReservationRepository $modeleReservationRepository,
        ReservationRepository $reservationRepository
    ): Response {

        $page = $request->query->getInt('page', 1);
        // // $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $modeleReservation = $modeleReservationRepository->paginateModele($page);
        $form = $this->createForm(CreationCoursType::class, null, ['list' => $modeleReservation]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $dateDebut = $data['dateFrom']->getTimestamp();
            $dateFin = $data['dateTo']->getTimestamp();
            $cours = $data['cours'];


            // dd($dateDebut, $dateFin, $cours);
            foreach ($cours as $modele) {
                //on récupère le jour de la semaine
                // $jour = $resa->getJour()->format('N');
                $jour = $modele->getJour();
                $annee = date('Y', $dateDebut);
                // dd($annee, $jour);
                //on récupère tous les jours de l'année correspondant au modèle
                $days = DateService::getAllDaysOfYear($annee, $jour);

                foreach ($days as $day) {
                    // dd(date('m/d/y', strtotime($day)), date("m/d/y", $dateDebut), date("m/d/y", $dateFin));
                    if (
                        date('m/d/y', strtotime($day)) >= date("m/d/y", $dateDebut) &&
                        date('m/d/y', strtotime($day)) <= date("m/d/y", $dateFin)
                    ) { // on regarde que l'on génère une date future.
                        //on regarde si le cours existe déjà pour ce coach sur cette journée et ces heures
                        $theDay = new \DateTime($day);
                        $heureDebut = new \DateTime(date_format($modele->getHeureDebut(), 'Y-m-d H:i:s'));
                        $heureFin = new \DateTime(date_format($modele->getHeureFin(), 'Y-m-d H:i:s'));

                        $newResa = $reservationRepository->findCours(
                            $heureDebut->format('H:i:s'),
                            $heureFin->format('H:i:s'),
                            $theDay->format('Y-m-d H:i:s'),
                            $modele->getCoach()
                        );
                        // dd($newResa);
                        if (!$newResa) {
                            $newResa = new Reservation;
                            //si la résa existe déjà en non active, on la laisse en état
                            $newResa->setIsActive(true);
                        }
                        $semaine = date("W", strtotime($day));
                        $newResa->setJour(new \DateTime($day));
                        $newResa->setSemaine($semaine);
                        $newResa->setHeureDebut($modele->getHeureDebut());
                        $newResa->setHeureFin($modele->getHeureFin());
                        $this->gestionNiveau($newResa, $modele);
                        // foreach ($modele->getNiveaux() as $niv)
                        //     $newResa->addNiveau($niv);
                        $newResa->setNombreMax($modele->getNombreMax());
                        $newResa->setNombreEncours(0);
                        $newResa->setCoach($modele->getCoach());
                        $newResa->setModele($modele);
                        $reservationRepository->save($newResa, true);
                    }
                }
            }
            /** TODO : 
             * Voir pour le chevauchement des heures
             * Voir pour travailler sur la seconde année
             */
            return $this->redirectToRoute('main');
            // dd($cour);
        }
        return $this->render('myAdmin/modele_reservation/generation.html.twig', compact('modeleReservation', 'form'));
    }

    private function gestionNiveau(Reservation $resa, ModeleReservation $modele)
    {
        // On commence par supprimer tous les niveaux de la réservation actuelle
        foreach ($resa->getNiveau() as $niv)
            $resa->removeNiveau($niv);
        foreach ($modele->getNiveaux() as $niv)
            $resa->addNiveau($niv);
    }
}
