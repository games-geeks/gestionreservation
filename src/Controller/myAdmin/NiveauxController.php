<?php

namespace App\Controller\myAdmin;

use App\Entity\Niveaux;
use App\Form\NiveauxType;
use App\Repository\NiveauxRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


#[Route('/administration/niveaux', name: 'myadmin.niveau.')]
class NiveauxController extends AbstractController
{
    #[Route('/', name: 'list', methods: ['GET'])]
    public function index(NiveauxRepository $niveauxRepository): Response
    {
        return $this->render('/myAdmin/niveaux/index.html.twig', [
            'niveaux' => $niveauxRepository->findAll(),
        ]);
    }

    #[Route('/creation', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, NiveauxRepository $niveauxRepository): Response
    {
        $niveau = new Niveaux();
        $form = $this->createForm(NiveauxType::class, $niveau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $niveauxRepository->save($niveau, true);

            return $this->redirectToRoute('myadmin.niveau.list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('myAdmin/niveaux/new.html.twig', [
            'niveau' => $niveau,
            'form' => $form,
        ]);
    }


    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(Request $request, Niveaux $niveau, NiveauxRepository $niveauxRepository): Response
    {
        $form = $this->createForm(NiveauxType::class, $niveau);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $niveauxRepository->save($niveau, true);

            return $this->redirectToRoute('myadmin.niveau.list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('myAdmin/niveaux/edit.html.twig', [
            'niveau' => $niveau,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    public function delete(Request $request, Niveaux $niveau, NiveauxRepository $niveauxRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $niveau->getId(), $request->request->get('_token'))) {
            $niveauxRepository->remove($niveau, true);
        }

        return $this->redirectToRoute('myadmin.niveau.list', [], Response::HTTP_SEE_OTHER);
    }
}
