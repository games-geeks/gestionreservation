<?php

namespace App\Controller\myAdmin;

use App\Entity\AbonnementsType;
use App\Form\AbonnementsTypeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use App\Repository\AbonnementsTypeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/administration/abonnements', name: 'myadmin.abonnement.')]

class AbonnementsTypeController extends AbstractController
{
    #[Route('/', name: 'list', methods: ['GET'])]
    public function index(Request $request, AbonnementsTypeRepository $abonnementsTypeRepository, Security $security): Response
    {
        $page = $request->query->getInt('page', 1);
        $abonnements_types = $abonnementsTypeRepository->paginateAbonnement($page);
        return $this->render('myAdmin/abonnements/index.html.twig', compact('abonnements_types'));
    }

    #[Route('/creation', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request, AbonnementsTypeRepository $abonnementsTypeRepository): Response
    {
        $abonnementsType = new AbonnementsType();
        $form = $this->createForm(AbonnementsTypeType::class, $abonnementsType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $abonnementsTypeRepository->save($abonnementsType, true);

            return $this->redirectToRoute('myadmin.abonnement.list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('myAdmin/abonnements/new.html.twig', [
            'abonnements_type' => $abonnementsType,
            'form' => $form,
        ]);
    }

    #[Route('/edition/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function edit(Request $request, AbonnementsType $abonnementsType, AbonnementsTypeRepository $abonnementsTypeRepository): Response
    {
        $form = $this->createForm(AbonnementsTypeType::class, $abonnementsType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $abonnementsTypeRepository->save($abonnementsType, true);

            return $this->redirectToRoute('myadmin.abonnement.list', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('myAdmin/abonnements/edit.html.twig', [
            'abonnements_type' => $abonnementsType,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE', 'POST'])]
    public function delete(Request $request, AbonnementsType $abonnementsType, EntityManagerInterface $em): Response
    {
        $message = 'L\'abonnement a été supprimé';
        if ($this->isCsrfTokenValid('delete' . $abonnementsType->getId(), $request->request->get('_token'))) {
            $em->remove($abonnementsType);
            $em->flush();
            $this->addFlash('success', $message);
        }

        return $this->redirectToRoute('myadmin.abonnement.list', [], Response::HTTP_SEE_OTHER);
    }
}
