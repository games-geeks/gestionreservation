<?php

namespace App\Controller;


use App\Entity\User;
use App\Entity\Reservation;
use App\Service\DateService;
use App\Service\SendMailService;
use App\Repository\UserRepository;
use App\Repository\JoueurRepository;
use App\Repository\StatutRepository;
use App\Entity\ReservationHistorique;
use App\Service\UpdateEncoursService;
use App\Repository\ReservationRepository;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Attribute\MapEntity;
use App\Repository\ReservationHistoriqueRepository;

use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ReservationController extends AbstractController
{

    public function __construct(private readonly UpdateEncoursService $ues)
    {
    }
    #[Route('/reservation', name: 'app_reservation')]
    public function index(): Response
    {
        return $this->render('reservation/index.html.twig', [
            'controller_name' => 'ReservationController',
        ]);
    }

    #[Route('/lundi', name: 'main_lundi')]
    public function lundi(): Response
    {
        $mondays = DateService::getAllDaysOfYear(2023, 2);
        dd($mondays);
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

    #[Route('/week/{nb}', name: 'voir_semaine',  methods: ['GET', 'POST'])]
    public function semaine(ReservationRepository $reservationRepository, Request $request)
    {

        $semaine =  $request->attributes->get('nb');
        $annee = date("Y");
        $cours = $reservationRepository->findBy(['semaine' => (int) $semaine, 'isActive' => true]);

        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'cours' => $cours,
            'semaine' => $semaine,
            'annee' => $annee
        ]);
    }


    // #[Route('/make_reservation/{id}', name: 'make_reservation',  methods: ['GET', 'POST'])]
    // public function makeReservation(ReservationRepository $reservationRepository, Reservation $reservation, SendMailService $mail): Response
    // {

    //     $addResa = true;
    //     //Utilisateur en cours
    //     $user = $this->getUser();
    //     $cours = $reservationRepository->findBy(['semaine' => (int) $reservation->getSemaine()]);
    //     //On regarde que l'on a le bon role
    //     $this->denyAccessUnlessGranted('ROLE_JOUEUR');

    //     //On vérifie que le nombre max de résa n'est pas atteint
    //     if ($reservation->getNombreEncours() == $reservation->getNombreMax()) {
    //         $this->addFlash('danger', 'Le quota de réservation a été atteint!');
    //         $addResa = false;
    //     }

    //     //on regarde si le joueur a déjà fait une resa pour ce cours
    //     if ($reservation->getJoueur()->contains($this->getUser())) {
    //         $this->addFlash('warning', 'Vous êtes déjà inscrit à ce cours!');
    //         $addResa = false;
    //     }

    //     //On controle que le joeur a le niveau adéquat
    //     if (!$reservation->getNiveau()->contains($user->getNiveau())) {
    //         $this->addFlash('danger', 'Vous n\'avez pas le niveau requis ou votre niveau est supérieur');
    //         $addResa = false;
    //     }

    //     if ($addResa) {
    //         //On peut créer la réservation
    //         $reservation->addJoueur($this->getUser());
    //         $reservation->setNombreEncours($reservation->getNombreEncours() + 1);
    //         $reservationRepository->save($reservation, true);
    //         //on envoie un mail

    //         $url = $this->generateUrl('info_cours', ['id' => $reservation->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
    //         $context = compact('user', 'reservation', 'url');
    //         $mail->send(
    //             'no-reply@us-cagnes.fr',
    //             $user->getEmail(),
    //             'Nouveau Rendez vous - Collectif Adulte ',
    //             'nouveau_rdv',
    //             $context
    //         );
    //         $this->addFlash('success', 'Réservation effectuée!');
    //         return  $this->redirectToRoute('voir_semaine', ['nb' => $reservation->getSemaine()]);
    //     }
    //     return $this->render('main/index.html.twig', [
    //         'controller_name' => 'MainController',
    //         'cours' => $cours,
    //         'semaine' => $reservation->getSemaine(),
    //         'annee' => ($reservation->getJour()->format('Y'))
    //     ]);


    //     //RAF : décrémenter le cours
    //     //Un coach peutt forcer un user

    // }

    #[Route('/make_reservation/{id}', name: 'make_reservation',  methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_JOUEUR', message: 'No access! Get out!')]
    public function makeReservation(
        ReservationRepository $reservationRepository,
        Reservation $reservation,
        SendMailService $mail,
        ReservationHistoriqueRepository $rh,
        StatutRepository $statutRepository,
        UserRepository $userRepository

    ): Response {

        //récupération du statut
        $statut = $statutRepository->findOneBy(['id' => 1]);
        $addResa = true;
        //Utilisateur en cours
        $user = $this->getUser();
        $cours = $reservationRepository->findBy(['semaine' => (int) $reservation->getSemaine()]);
        //On regarde que l'on a le bon role
        $this->denyAccessUnlessGranted('ROLE_JOUEUR');

        //On regarde que notre compte soit bien validé
        if (!$user->IsVerified()) {
            $this->addFlash('danger', 'Le compte doit être validé!');
            $addResa = false;
        }

        //On vérifie que le nombre max de résa n'est pas atteint
        if ($reservation->getNombreEncours() == $reservation->getNombreMax()) {
            $this->addFlash('danger', 'Le quota de réservation a été atteint!');
            $addResa = false;
        }

        //on regarde si le joueur a déjà fait une resa pour ce cours
        if ($reservation->getJoueur()->contains($this->getUser())) {
            $this->addFlash('warning', 'Vous êtes déjà inscrit à ce cours!');
            $addResa = false;
        }

        //On controle que le joueur a le niveau adéquat
        if (!$reservation->getNiveau()->contains($user->getNiveau())) {
            $this->addFlash('danger', 'Vous n\'avez pas le niveau requis ou votre niveau est supérieur');
            $addResa = false;
        }

        //on regarde si le crédit de cours est suffisant

        // if ($user->getNombreLeconsRestantes() < 1) {
        //     $this->addFlash('danger', 'Vous n\'avez pas accès de crédit pour effectuer cette réservation');
        //     $addResa = false;
        // }

        if ($addResa) {
            //On peut créer la réservation
            $reservation->addJoueur($this->getUser());
            $reservation->setNombreEncours($reservation->getNombreEncours() + 1);
            $reservationRepository->save($reservation, true);
            //et on la rajoute (ou met à jour ) dans l'historique
            $historique = $rh->findOneBy(['joueur' => $user, 'reservation' => $reservation]);
            if ($historique) {
                $historique->setOrigine($user);
                $historique->setStatut($statut);
                $rh->save($historique, true);
            } else {
                $historique = new ReservationHistorique;
                $historique->setJoueur($user);
                $historique->setReservation($reservation);
                $historique->setOrigine($user);
                $historique->setStatut($statut);
                $rh->save($historique, true);
            }
            //on maj le nombre de leçon restantes
            // $user->setNombreLeconsRestantes($user->getNombreLeconsRestantes() - 1);
            // $userRepository->save($user, true);
            //on envoie un mail
            $url = $this->generateUrl('info_cours', ['id' => $reservation->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
            $context = compact('user', 'reservation', 'url');
            $mail->send(
                'no-reply@us-cagnes.fr',
                $user->getEmail(),
                'Nouveau Rendez vous - Collectif Adulte ',
                'nouveau_rdv',
                $context
            );
            $this->addFlash('success', 'Réservation effectuée!');
            return  $this->redirectToRoute('voir_semaine', ['nb' => $reservation->getSemaine()]);
        }
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'cours' => $cours,
            'semaine' => $reservation->getSemaine(),
            'annee' => ($reservation->getJour()->format('Y'))
        ]);


        //RAF : décrémenter le cours
        //Un coach peutt forcer un user

    }
    #[Route('/cancel_reservation/{id}', name: 'cancel_reservation',  methods: ['GET', 'POST'])]
    public function cancelReservation(
        ReservationRepository $reservationRepository,
        Reservation $reservation,
        SendMailService $mail,
        ReservationHistoriqueRepository $rh,
        StatutRepository $statutRepository,
        UserRepository $userRepository
    ): Response {
        //Utilisateur en cours
        $user = $this->getUser();

        //récupération du statut
        $statut = $statutRepository->findOneBy(['id' => 2]);
        //On regarde que l'on a le bon role
        $this->denyAccessUnlessGranted('ROLE_JOUEUR');
        //On peut supprimer la réservation
        $reservation->removeJoueur($user);
        $reservation->setNombreEncours($reservation->getNombreEncours() - 1);
        $reservationRepository->save($reservation, true);
        //et on la rajoute (ou met à jour ) dans l'historique
        $historique = $rh->findOneBy(['joueur' => $user, 'reservation' => $reservation]);
        if ($historique) {
            $historique->setOrigine($user);
            $historique->setStatut($statut);
            $rh->save($historique, true);
        }
        //on recrédite le nombre de leçon restantes
        // $user->setNombreLeconsRestantes($user->getNombreLeconsRestantes() + 1);
        // $userRepository->save($user, true);

        //on envoie un mail
        $url = $this->generateUrl('info_cours', ['id' => $reservation->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $context = compact('user', 'reservation', 'url');
        $mail->send(
            'no-reply@us-cagnes.fr',
            $user->getEmail(),
            'Annulation Rendez vous - Collectif Adulte ',
            'annulation_rdv',
            $context
        );
        $this->addFlash('success', 'Annulation confirmée!');
        return  $this->redirectToRoute('voir_semaine', ['nb' => $reservation->getSemaine()]);
    }

    #[Route('/cancel_reservation_coach/{user_id}/{reservation_id}', name: 'cancel_reservation_coach',  methods: ['GET', 'POST'])]
    // #[ParamConverter('user', class: 'App\Entity\User', options: ['mapping' => ['user_id' => 'id']])]
    // #[ParamConverter('reservation', class: 'App\Entity\Reservation', options: ['mapping' => ['reservation_id' => 'id']])]
    #[IsGranted('ROLE_COACH', message: 'No access! Get out!')]
    public function cancelReservationbyCoach(
        ReservationRepository $reservationRepository,
        #[MapEntity(id: 'reservation_id')] Reservation $reservation,
        #[MapEntity(id: 'user_id')] User $user,
        SendMailService $mail,
        ReservationHistoriqueRepository $rh,
        StatutRepository $statutRepository,
        JoueurRepository $joueurRepository
    ): Response {

        //récupération du statut
        //1 =>Success
        //2 =>Annulée
        $statut = $statutRepository->findOneBy(['id' => 2]);
        //info du coach
        $coach = $this->getUser();
        //On regarde que l'on a le bon role
        // $this->denyAccessUnlessGranted('ROLE_COACH');
        //On peut créer la réservation
        $reservation->removeJoueur($user);
        $reservation->setNombreEncours($reservation->getNombreEncours() - 1);
        $reservationRepository->save($reservation, true);
        //et on la rajoute (ou met à jour ) dans l'historique
        $historique = $rh->findOneBy(['joueur' => $user, 'reservation' => $reservation]);
        if ($historique) {
            $historique->setOrigine($coach);
            $historique->setStatut($statut);
            $rh->save($historique, true);
        }
        //on recrédite le nombre de leçon restantes seulement si la date du cours est dans le passé
        $this->ues->checkIfResaIsInPast($reservation, $user, $joueurRepository);

        //on envoie un mail
        $url = $this->generateUrl('info_cours', ['id' => $reservation->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
        $context = compact('user', 'reservation', 'url', 'coach');
        $mail->send(
            'no-reply@us-cagnes.fr',
            $user->getEmail(),
            'Annulation Rendez vous - Collectif Adulte ',
            'annulation_rdv_coach',
            $context
        );
        return $this->redirectToRoute('info_cours', ['id' => $reservation->getId()]);
        // return $this->render('reservation/show.html.twig', [
        //     'cours' => $reservation,
        //     'semaine' => $reservation->getSemaine(),
        //     'annee' => ($reservation->getJour()->format('Y'))
        // ]);
    }

    #[Route('/cancel_cours_coach/{id}', name: 'cancel_cours_coach',  methods: ['GET', 'POST'])]
    // #[ParamConverter('reservation', class: 'App\Entity\Reservation', options: ['mapping' => ['reservation_id' => 'id']])]
    #[IsGranted('ROLE_COACH', message: 'No access! Get out!')]
    public function cancelCoursByCoach(
        ReservationRepository $reservationRepository,
        Reservation $reservation,
        SendMailService $mail,
        ReservationHistoriqueRepository $rh,
        StatutRepository $statutRepository,
        JoueurRepository $joueurRepository

    ): Response {

        /**
         * On doit
         * Prévenir par mail
         * Passer le cours en annulé (non visible)
         * Créditer les sessions
         * 
         */
        //récupération du statut
        //1 =>Success
        //2 =>Annulée
        $statut = $statutRepository->findOneBy(['id' => 2]);
        //info du coach
        $coach = $this->getUser();

        $joueurs = $reservation->getJoueur();
        foreach ($joueurs as $user) {
            //On peut supprimer la réservation
            $reservation->removeJoueur($user);
            $reservation->setNombreEncours($reservation->getNombreEncours() - 1);

            //et on la rajoute (ou met à jour ) dans l'historique
            $historique = $rh->findOneBy(['joueur' => $user, 'reservation' => $reservation]);
            if ($historique) {
                $historique->setOrigine($coach);
                $historique->setStatut($statut);
                $rh->save($historique, true);
            }
            //on recrédite le nombre de leçon restantes seulement si la date du cours est dans le passé
            $this->ues->checkIfResaIsInPast($reservation, $user, $joueurRepository);

            //on envoie un mail
            $url = $this->generateUrl('info_cours', ['id' => $reservation->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
            $context = compact('user', 'reservation', 'url', 'coach');
            $mail->send(
                'no-reply@us-cagnes.fr',
                $user->getEmail(),
                'Annulation Rendez vous - Collectif Adulte ',
                'annulation_rdv_coach',
                $context
            );
        }
        $reservation->setIsActive(false);
        $reservationRepository->save($reservation, true);
        return  $this->redirectToRoute('voir_semaine', ['nb' => $reservation->getSemaine()]);
    }


    #[IsGranted('ROLE_USER', message: 'No access! Get out!')]
    #[Route('/info_cours/{id}', name: 'info_cours',  methods: ['GET', 'POST'])]
    public function infoCours(Reservation $reservation): Response
    {
        return $this->render('reservation/show.html.twig', [
            'cours' => $reservation,
            'semaine' => $reservation->getSemaine(),
            'annee' => ($reservation->getJour()->format('Y'))
        ]);
    }

    #[Route('mes_resarvations', name: 'my_reservation')]
    public function myResa(UserRepository $userRepository): Response
    {

        //Utilisateur en cours
        //On regarde que l'on a le bon role
        $this->denyAccessUnlessGranted('ROLE_JOUEUR', message: 'Vous n\'avez pas l\'autorisation');

        return $this->render('reservation/list.html.twig', []);
    }
}
