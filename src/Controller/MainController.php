<?php

namespace App\Controller;

use App\Repository\ReservationRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    #[Route('/', name: 'main')]
    public function index(ReservationRepository $reservationRepository): Response
    {
        $semaine = date("W");
        $annee = date("Y");

        $cours = $reservationRepository->findBy(['semaine' => (int) $semaine, 'isActive' => true]);

        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'cours' => $cours,
            'semaine' => $semaine,
            'annee' => $annee
        ]);
    }
}
