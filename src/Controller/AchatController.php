<?php

namespace App\Controller;

use LogicException;
use App\Entity\Achats;
use App\Service\Securizer;
use App\Entity\AbonnementsType;
use App\Service\SendMailService;
use App\Repository\AchatsRepository;
use Knp\Component\Pager\PaginatorInterface;
use App\Repository\AbonnementsTypeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Workflow\WorkflowInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


#[Route('/achat', name: 'achat_')]
class AchatController extends AbstractController
{


    public function __construct(
        private WorkflowInterface $achatRequestWorkflow
    ) {
    }

    #[Route('/liste', name: 'liste')]
    public function index(AbonnementsTypeRepository $abonnementsTypeRepository): Response
    {
        $abonnements = $abonnementsTypeRepository->findAll([]);


        return $this->render('achat/index.html.twig', [
            'abonnements' => $abonnements,
            'controller_name' => 'AchatController',
        ]);
    }

    #[Route('/nouveau/{id}', name: 'nouveau')]
    // #[ParamConverter('abonnementsType', class: 'App\Entity\AbonnementsType', options: ['mapping' => ['id' => 'id']])]
    public function nouvelAchat(AbonnementsType $abonnementsType, AchatsRepository $achatsRepository, SendMailService $mail,)
    {
        //Utilisateur en cours
        $user = $this->getUser();
        //On regarde que l'on a le bon role
        $this->denyAccessUnlessGranted('ROLE_JOUEUR');
        $achat = new Achats;
        $achat->setJoueur($user);
        $achat->setAbonnement($abonnementsType);
        $achat->setPrixAbonnement($abonnementsType->getTarif());
        $achat->setPrixPaye($abonnementsType->getTarif());
        $achatsRepository->save($achat, true);
        //on envoie le mail pour confirmation
        $url = $this->generateUrl('achat_status', [], UrlGeneratorInterface::ABSOLUTE_URL);
        $context = compact('user', 'achat', 'url');

        $mail->send(
            'no-reply@gestionReservation.fr',
            $user->getEmail(),
            'Nouvelle demande pour un achat ',
            'workflow/nouvel_achat',
            $context
        );
        //on peut lancer le workflow
        // try {
        //     $this->achatRequestWorkflow->apply($achat, 'to_coach_validation_pending');
        // } catch (LogicException $exception) {
        //     //
        // }

        return  $this->redirectToRoute('achat_status');
    }

    #[Route('/status', name: 'status')]
    public function status(PaginatorInterface $paginator, Request $request, AchatsRepository $achatsRepository, Securizer $securizer): Response
    {
        //Utilisateur en cours
        $user = $this->getUser();

        //On regarde que l'on a le bon role
        if (!($securizer->isGranted($user, 'ROLE_COACH')) && !($securizer->isGranted($user, 'ROLE_JOUEUR'))) {
            $this->denyAccessUnlessGranted(['ROLE_COACH', 'ROLE_JOUEUR']);
        }
        //on construit la requête selon si on est Coach ou Joueur
        if ($securizer->isGranted($user, 'ROLE_COACH')) {
            $lachats = $achatsRepository->findBy([], ['updatedAt' => 'DESC']);
        }
        if ($securizer->isGranted($user, 'ROLE_JOUEUR')) {
            $lachats = $achatsRepository->findBy(['joueur' => $user], ['updatedAt' => 'DESC']);
        }
        //on crée la pagination
        $achats = $paginator->paginate(
            // Doctrine Query, not results
            $lachats,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            30
        );

        //et on la paramètre
        $achats->setCustomParameters([
            'align' => 'center', # center|right (for template: twitter_bootstrap_v4_pagination)
            'size' => 'small', # small|large (for template: twitter_bootstrap_v4_pagination)
            'style' => 'bottom',
            'span_class' => 'whatever',
        ]);
        return $this->render('achat/status.html.twig', [
            'achats' => $achats,
        ]);
    }


    #[Route('/change/{id}/{to}', name: 'change')]
    public function change(Achats $achats, String $to, AchatsRepository $achatsRepository): Response
    {
        try {
            $this->achatRequestWorkflow->apply($achats, $to);
        } catch (LogicException $exception) {
            //
        }
        $achatsRepository->save($achats, true);

        $this->addFlash('success', 'Action enregistrée !');
        return $this->redirectToRoute('achat_status');
    }
}
