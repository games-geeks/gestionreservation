<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;

use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class UserCrudController extends AbstractCrudController
{
    use Trait\UserTrait;
    public static function getEntityFqcn(): string
    {
        return User::class;
    }
    public function configureFilters(Filters $filters): Filters
    {

        return $filters
            ->add('prenom')
            ->add('nom');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email'),
            //TextField::new('password')->setFormType(PasswordType::class),
            TextField::new('prenom'),
            DateField::new('dateNaissance'),
            TextField::new('nom'),
            TextField::new('adresse'),
            TextField::new('adresseComplementaire'),
            TelephoneField::new('telephone'),
            AssociationField::new('localite'),
            AssociationField::new('niveau'),
            IntegerField::new('nombreLeconsRestantes'),
            // TextField::new('couleur'),
            ChoiceField::new('couleur')->setChoices([
                'Bleu' => 'blue',
                'Indigo' => 'indigo',
                'Purple' => 'purple',
            ]),
            ArrayField::new('roles'),

        ];
    }
}
