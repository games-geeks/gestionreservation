<?php

namespace App\Controller\Admin;

use App\Entity\Reservation;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ReservationCrudController extends AbstractCrudController
{

    public const ACTION_DUPLICATE = 'duplicate';

    public static function getEntityFqcn(): string
    {
        return Reservation::class;
    }
    public function configureActions(Actions $actions): Actions
    {
        $duplicate = Action::new(self::ACTION_DUPLICATE)
            ->linkToCrudAction('duplicateProduct')
            ->setCssClass('btn btn-info');
        return $actions
            ->add(Crud::PAGE_EDIT, $duplicate)
            ->reorder(Crud::PAGE_EDIT, [self::ACTION_DUPLICATE, Action::SAVE_AND_RETURN]);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateField::new('jour')->setFormat('dd MMM YYYY'),
            TimeField::new('heureDebut')->setFormat('HH:mm'),
            TimeField::new('heureFin')->setFormat('HH:mm'),
            AssociationField::new('niveau')->hideOnIndex(),
            ArrayField::new('niveau')->hideOnForm(),
            IntegerField::new('nombreMax'),
            AssociationField::new('coach')->setFormTypeOptions(
                [
                    'query_builder' => function (UserRepository $entityRepository) {
                        return $entityRepository->createQueryBuilder('e')
                            ->where("e.roles like '%ROLE_COACH%'")
                            ->orderBy('e.nom', 'ASC');
                    }
                ]
            ),
            IntegerField::new('semaine'),
            IntegerField::new('day'),
            BooleanField::new('isModele'),
            BooleanField::new('isActive')

        ];
    }


    public function duplicateProduct(
        AdminContext $adminContext,
        AdminUrlGenerator $adminUrlGenerator,
        EntityManagerInterface $em
    ): Response {
        /** @var Reservation $reservation */
        $reservation = $adminContext->getEntity()->getInstance();
        $duplicateReservation = clone $reservation;

        parent::persistEntity($em, $duplicateReservation);
        $url = $adminUrlGenerator->setController(self::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($duplicateReservation->getId())
            ->generateUrl();

        return $this->redirect($url);
    }
}
