<?php

namespace App\Controller\Admin\Trait;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

trait UserTrait
{
    public function configureActions(Actions $actions): Actions
    {
        $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL); // sur l'accueil on ajoute le show

        return $actions;
    }
}

            //->disable(Action::NEW, Action::EDIT, Action::DELETE) // ons supprime les boutons
