<?php

namespace App\Controller\Admin;

use App\Entity\AbonnementsType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AbonnementsTypeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AbonnementsType::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('nom'),
            TextEditorField::new('description'),
            MoneyField::new('tarif')->setCurrency('EUR')->setNumDecimals(2),
            IntegerField::new('nombreLecon'),
        ];
    }
}
