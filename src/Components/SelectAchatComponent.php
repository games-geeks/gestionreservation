<?php

namespace App\Components;

use App\Entity\AbonnementsType;
use App\Entity\Centres;
use App\Repository\AbonnementsTypeRepository;
use App\Repository\CentresRepository;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\LiveComponent\ValidatableComponentTrait;

#[AsLiveComponent('select_achat')]
class SelectAchatComponent
{
    use DefaultActionTrait;

    use ValidatableComponentTrait;

    #[LiveProp(writable: true, exposed: ['nom', 'abonnementsTypes'])]
    public ?Centres $centres;

    public function __construct(private CentresRepository $centreRepository, private AbonnementsTypeRepository $abonnementsTypeRepository)
    {
    }

    /**
     * @return Centres[]
     */

    public function getAllCentres(): array
    {

        return $this->centreRepository->findAll();
    }

    /**
     * @return AbonnementsType[]
     */

    public function getAllAbonnements(): array
    {
        echo $this->centres->getId();
        return $this->abonnementsTypeRepository->findBy(['id' => $this->centres->getId()]);
    }
}
