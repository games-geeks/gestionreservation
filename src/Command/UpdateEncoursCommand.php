<?php

namespace App\Command;


use App\Service\UpdateEncoursService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:update-encours',
    description: 'Update encours after a day',
)]
class UpdateEncoursCommand extends Command
{
    public function __construct(private readonly UpdateEncoursService $updateEncoursService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        // $this
        //     ->addArgument('email', InputArgument::REQUIRED, 'email of admin user')
        //     ->addArgument('password', InputArgument::REQUIRED, 'password of admin user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        $this->updateEncoursService->updateEncours();

        $io->success('Successfuly created admin user.');

        return Command::SUCCESS;
    }
}
