<?php

namespace App\Command;

use App\Service\ImportAbonnementsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:import-abonnements')]

class ImportAbonnementsCommand extends Command
{

    public function __construct(private ImportAbonnementsService $importAbonnementsService)
    {
        parent::__construct();
    }
    protected function execute(InputInterface $input,  OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->importAbonnementsService->importAbonnements($io);

        return Command::SUCCESS;
    }
}
