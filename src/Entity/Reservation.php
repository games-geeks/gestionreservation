<?php

namespace App\Entity;

use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $jour = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $heureDebut = null;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private ?\DateTimeInterface $heureFin = null;

    #[ORM\ManyToMany(targetEntity: Niveaux::class, inversedBy: 'reservations')]
    private Collection $niveau;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?Coach $coach = null;

    #[ORM\Column(options: ["default" => 6])]
    private ?int $nombreMax = 6;

    #[ORM\Column(nullable: true, options: ["default" => 0])]
    private ?int $nombreEncours = 0;

    #[ORM\ManyToMany(targetEntity: Joueur::class, inversedBy: 'cours')]
    private Collection $joueur;

    #[ORM\Column(nullable: true)]
    private ?int $semaine = null;

    #[ORM\Column(nullable: true)]
    private ?bool $isActive = true;

    #[ORM\OneToMany(mappedBy: 'reservation', targetEntity: ReservationHistorique::class)]
    private Collection $reservationHistoriques;

    #[ORM\ManyToOne(inversedBy: 'reservations')]
    private ?ModeleReservation $modele = null;

    public function __construct()
    {
        $this->niveau = new ArrayCollection();
        $this->joueur = new ArrayCollection();
        $this->reservationHistoriques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJour(): ?\DateTimeInterface
    {
        return $this->jour;
    }

    public function setJour(\DateTimeInterface $jour): self
    {
        $this->jour = $jour;

        return $this;
    }

    public function getHeureDebut(): ?\DateTimeInterface
    {
        return $this->heureDebut;
    }

    public function setHeureDebut(\DateTimeInterface $heureDebut): self
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    public function getHeureFin(): ?\DateTimeInterface
    {
        return $this->heureFin;
    }

    public function setHeureFin(\DateTimeInterface $heureFin): self
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * @return Collection<int, Niveaux>
     */
    public function getNiveau(): Collection
    {
        return $this->niveau;
    }

    public function addNiveau(Niveaux $niveau): self
    {
        if (!$this->niveau->contains($niveau)) {
            $this->niveau->add($niveau);
        }

        return $this;
    }

    public function removeNiveau(Niveaux $niveau): self
    {
        $this->niveau->removeElement($niveau);

        return $this;
    }

    public function getCoach(): ?Coach
    {
        return $this->coach;
    }

    public function setCoach(?Coach $coach): self
    {
        $this->coach = $coach;

        return $this;
    }

    public function getNombreMax(): ?int
    {
        return $this->nombreMax;
    }

    public function setNombreMax(int $nombreMax): self
    {
        $this->nombreMax = $nombreMax;

        return $this;
    }

    public function getNombreEncours(): ?int
    {
        return $this->nombreEncours;
    }

    public function setNombreEncours(int $nombreEncours): self
    {
        $this->nombreEncours = $nombreEncours;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getJoueur(): Collection
    {
        return $this->joueur;
    }

    public function addJoueur(Joueur $joueur): self
    {
        if (!$this->joueur->contains($joueur)) {
            $this->joueur->add($joueur);
        }

        return $this;
    }

    public function removeJoueur(Joueur $joueur): self
    {
        $this->joueur->removeElement($joueur);

        return $this;
    }

    public function getSemaine(): ?int
    {
        return $this->semaine;
    }

    public function setSemaine(?int $semaine): self
    {
        $this->semaine = $semaine;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection<int, ReservationHistorique>
     */
    public function getReservationHistoriques(): Collection
    {
        return $this->reservationHistoriques;
    }

    public function addReservationHistorique(ReservationHistorique $reservationHistorique): self
    {
        if (!$this->reservationHistoriques->contains($reservationHistorique)) {
            $this->reservationHistoriques->add($reservationHistorique);
            $reservationHistorique->setReservation($this);
        }

        return $this;
    }

    public function removeReservationHistorique(ReservationHistorique $reservationHistorique): self
    {
        if ($this->reservationHistoriques->removeElement($reservationHistorique)) {
            // set the owning side to null (unless already changed)
            if ($reservationHistorique->getReservation() === $this) {
                $reservationHistorique->setReservation(null);
            }
        }

        return $this;
    }

    public function getModele(): ?ModeleReservation
    {
        return $this->modele;
    }

    public function setModele(?ModeleReservation $modele): static
    {
        $this->modele = $modele;

        return $this;
    }
}
