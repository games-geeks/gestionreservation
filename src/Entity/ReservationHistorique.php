<?php

namespace App\Entity;

use App\Repository\ReservationHistoriqueRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReservationHistoriqueRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ReservationHistorique
{

    use Timestampable;


    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'reservationHistoriques')]
    private ?Reservation $reservation = null;

    #[ORM\Id]
    #[ORM\ManyToOne(inversedBy: 'reservationHistoriques')]
    private ?User $joueur = null;

    #[ORM\ManyToOne(inversedBy: 'origineStatut')]
    private ?User $origine = null;

    #[ORM\ManyToOne(inversedBy: 'reservationHistoriques')]
    private ?Statut $statut = null;



    public function getReservation(): ?Reservation
    {
        return $this->reservation;
    }

    public function setReservation(?Reservation $reservation): self
    {
        $this->reservation = $reservation;

        return $this;
    }

    public function getJoueur(): ?User
    {
        return $this->joueur;
    }

    public function setJoueur(?User $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getOrigine(): ?User
    {
        return $this->origine;
    }

    public function setOrigine(?User $origine): self
    {
        $this->origine = $origine;

        return $this;
    }

    public function getStatut(): ?Statut
    {
        return $this->statut;
    }

    public function setStatut(?Statut $statut): self
    {
        $this->statut = $statut;

        return $this;
    }
}
