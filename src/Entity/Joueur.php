<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\JoueurRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: JoueurRepository::class)]
class Joueur extends User
{
    #[ORM\Column]
    private ?int $nombreLeconsRestantes = 0;

    #[ORM\OneToMany(mappedBy: 'joueur', targetEntity: Achats::class)]
    private Collection $achats;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateNaissance = null;

    #[ORM\ManyToOne(inversedBy: 'users')]
    private ?Niveaux $niveau = null;


    #[ORM\ManyToMany(targetEntity: Reservation::class, mappedBy: 'joueur')]
    private Collection $cours;

    #[ORM\OneToMany(mappedBy: 'joueur', targetEntity: ReservationHistorique::class)]
    private Collection $reservationHistoriques;

    #[ORM\OneToMany(mappedBy: 'origine', targetEntity: ReservationHistorique::class)]
    private Collection $origineStatut;

    public function __construct()
    {
        $this->achats = new ArrayCollection();

        $this->cours = new ArrayCollection();
        $this->reservationHistoriques = new ArrayCollection();
        $this->origineStatut = new ArrayCollection();
    }
    /**
     * @return Collection<int, Achats>
     */
    public function getAchats(): Collection
    {
        return $this->achats;
    }

    public function addAchat(Achats $achat): self
    {
        if (!$this->achats->contains($achat)) {
            $this->achats->add($achat);
            $achat->setJoueur($this);
        }

        return $this;
    }

    public function removeAchat(Achats $achat): self
    {
        if ($this->achats->removeElement($achat)) {
            // set the owning side to null (unless already changed)
            if ($achat->getJoueur() === $this) {
                $achat->setJoueur(null);
            }
        }

        return $this;
    }

    public function getDateNaissance(): ?\DateTimeInterface
    {
        return $this->dateNaissance;
    }

    public function setDateNaissance(?\DateTimeInterface $dateNaissance): self
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    public function getNiveau(): ?Niveaux
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveaux $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    // getAge() outputs something like this: '1 years, 1 months, 8 days old.'
    public function getAge()
    {
        $now = new \DateTime('now');
        $age = $this->getDateNaissance();
        $difference = $now->diff($age);

        //return $difference->format('%y years, %m months, %d days old.');
        return $difference->format('%y ans');
    }



    /**
     * @return Collection<int, Reservation>
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Reservation $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours->add($cour);
            $cour->addJoueur($this);
        }

        return $this;
    }

    public function removeCour(Reservation $cour): self
    {
        if ($this->cours->removeElement($cour)) {
            $cour->removeJoueur($this);
        }

        return $this;
    }
    /**
     * @return Collection<int, ReservationHistorique>
     */
    public function getReservationHistoriques(): Collection
    {
        return $this->reservationHistoriques;
    }

    public function addReservationHistorique(ReservationHistorique $reservationHistorique): self
    {
        if (!$this->reservationHistoriques->contains($reservationHistorique)) {
            $this->reservationHistoriques->add($reservationHistorique);
            $reservationHistorique->setJoueur($this);
        }

        return $this;
    }

    public function removeReservationHistorique(ReservationHistorique $reservationHistorique): self
    {
        if ($this->reservationHistoriques->removeElement($reservationHistorique)) {
            // set the owning side to null (unless already changed)
            if ($reservationHistorique->getJoueur() === $this) {
                $reservationHistorique->setJoueur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ReservationHistorique>
     */
    public function getOrigineStatut(): Collection
    {
        return $this->origineStatut;
    }

    public function addOrigineStatut(ReservationHistorique $origineStatut): self
    {
        if (!$this->origineStatut->contains($origineStatut)) {
            $this->origineStatut->add($origineStatut);
            $origineStatut->setOrigine($this);
        }

        return $this;
    }

    public function removeOrigineStatut(ReservationHistorique $origineStatut): self
    {
        if ($this->origineStatut->removeElement($origineStatut)) {
            // set the owning side to null (unless already changed)
            if ($origineStatut->getOrigine() === $this) {
                $origineStatut->setOrigine(null);
            }
        }

        return $this;
    }


    public function getNombreLeconsRestantes(): ?int
    {
        return $this->nombreLeconsRestantes;
    }

    public function setNombreLeconsRestantes(int $nombreLeconsRestantes): self
    {
        $this->nombreLeconsRestantes = $nombreLeconsRestantes;

        return $this;
    }
}
