<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\AchatsRepository;

#[ORM\Entity(repositoryClass: AchatsRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Achats
{
    use Timestampable;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'achats')]
    private ?User $joueur = null;

    #[ORM\ManyToOne(inversedBy: 'achats')]
    private ?AbonnementsType $abonnement = null;

    #[ORM\Column]
    private ?int $prixPaye = null;

    #[ORM\Column(nullable: true)]
    private ?int $remise = null;

    #[ORM\Column]
    private ?int $prixAbonnement = null;

    #[ORM\Column(type: Types::ARRAY)]
    private array $status = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoueur(): ?User
    {
        return $this->joueur;
    }

    public function setJoueur(?User $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getAbonnement(): ?AbonnementsType
    {
        return $this->abonnement;
    }

    public function setAbonnement(?AbonnementsType $abonnement): self
    {
        $this->abonnement = $abonnement;

        return $this;
    }

    public function getPrixPaye(): ?int
    {
        return $this->prixPaye;
    }

    public function setPrixPaye(int $prixPaye): self
    {
        $this->prixPaye = $prixPaye;

        return $this;
    }

    public function getRemise(): ?int
    {
        return $this->remise;
    }

    public function setRemise(?int $remise): self
    {
        $this->remise = $remise;

        return $this;
    }

    public function getPrixAbonnement(): ?int
    {
        return $this->prixAbonnement;
    }

    public function setPrixAbonnement(int $prixAbonnement): self
    {
        $this->prixAbonnement = $prixAbonnement;

        return $this;
    }

    public function getStatus(): array
    {
        return $this->status;
    }

    public function setStatus(array $status): self
    {
        $this->status = $status;

        return $this;
    }
}
