<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\NiveauxRepository;

#[ORM\Entity(repositoryClass: NiveauxRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Niveaux
{
    use Timestampable;


    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\OneToMany(mappedBy: 'niveau', targetEntity: Joueur::class)]
    private Collection $users;

    #[ORM\ManyToMany(targetEntity: Reservation::class, mappedBy: 'niveau')]
    private Collection $reservations;

    #[ORM\ManyToMany(targetEntity: ModeleReservation::class, mappedBy: 'niveaux')]
    private Collection $modeleReservations;

    #[ORM\Column(nullable: true)]
    private ?int $level = null;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->reservations = new ArrayCollection();
        $this->modeleReservations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(Joueur $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setNiveau($this);
        }

        return $this;
    }

    public function removeUser(Joueur $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getNiveau() === $this) {
                $user->setNiveau(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getNom();
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->addNiveau($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            $reservation->removeNiveau($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, ModeleReservation>
     */
    public function getModeleReservations(): Collection
    {
        return $this->modeleReservations;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): static
    {
        $this->level = $level;

        return $this;
    }
}
