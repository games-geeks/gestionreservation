<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CityRepository::class)]
class City
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $inseeCode = null;

    #[ORM\Column(length: 255)]
    private ?string $cityCode = null;

    #[ORM\Column(length: 255)]
    private ?string $zipCode = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $label = null;

    #[ORM\Column(length: 255)]
    private ?string $latitude = null;

    #[ORM\Column(length: 255)]
    private ?string $longitude = null;

    #[ORM\Column(length: 255)]
    private ?string $departmentName = null;

    #[ORM\Column(length: 255)]
    private ?string $departmentNumber = null;

    #[ORM\Column(length: 255)]
    private ?string $regionName = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $regionGeoJsonName = null;

    #[ORM\OneToMany(mappedBy: 'localite', targetEntity: Centres::class)]
    private Collection $centres;

    #[ORM\OneToMany(mappedBy: 'localite', targetEntity: User::class)]
    private Collection $users;

    public function __construct()
    {
        $this->personnes = new ArrayCollection();
        $this->centres = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInseeCode(): ?string
    {
        return $this->inseeCode;
    }

    public function setInseeCode(string $inseeCode): self
    {
        $this->inseeCode = $inseeCode;

        return $this;
    }

    public function getCityCode(): ?string
    {
        return $this->cityCode;
    }

    public function setCityCode(string $cityCode): self
    {
        $this->cityCode = $cityCode;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDepartmentName(): ?string
    {
        return $this->departmentName;
    }

    public function setDepartmentName(string $departmentName): self
    {
        $this->departmentName = $departmentName;

        return $this;
    }

    public function getDepartmentNumber(): ?string
    {
        return $this->departmentNumber;
    }

    public function setDepartmentNumber(string $departmentNumber): self
    {
        $this->departmentNumber = $departmentNumber;

        return $this;
    }

    public function getRegionName(): ?string
    {
        return $this->regionName;
    }

    public function setRegionName(string $regionName): self
    {
        $this->regionName = $regionName;

        return $this;
    }

    public function getRegionGeoJsonName(): ?string
    {
        return $this->regionGeoJsonName;
    }

    public function setRegionGeoJsonName(string $regionGeoJsonName): self
    {
        $this->regionGeoJsonName = $regionGeoJsonName;

        return $this;
    }


    public function __toString()
    {
        return $this->getZipCode() . ' - ' . $this->getLabel();
    }

    /**
     * @return Collection<int, Centres>
     */
    public function getCentres(): Collection
    {
        return $this->centres;
    }

    public function addCentre(Centres $centre): self
    {
        if (!$this->centres->contains($centre)) {
            $this->centres->add($centre);
            $centre->setLocalite($this);
        }

        return $this;
    }

    public function removeCentre(Centres $centre): self
    {
        if ($this->centres->removeElement($centre)) {
            // set the owning side to null (unless already changed)
            if ($centre->getLocalite() === $this) {
                $centre->setLocalite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setLocalite($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getLocalite() === $this) {
                $user->setLocalite(null);
            }
        }

        return $this;
    }
}
