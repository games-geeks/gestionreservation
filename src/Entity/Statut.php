<?php

namespace App\Entity;

use App\Repository\StatutRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StatutRepository::class)]
class Statut
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $couleur = null;

    #[ORM\OneToMany(mappedBy: 'statut', targetEntity: ReservationHistorique::class)]
    private Collection $reservationHistoriques;

    public function __construct()
    {
        $this->reservationHistoriques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * @return Collection<int, ReservationHistorique>
     */
    public function getReservationHistoriques(): Collection
    {
        return $this->reservationHistoriques;
    }

    public function addReservationHistorique(ReservationHistorique $reservationHistorique): self
    {
        if (!$this->reservationHistoriques->contains($reservationHistorique)) {
            $this->reservationHistoriques->add($reservationHistorique);
            $reservationHistorique->setStatut($this);
        }

        return $this;
    }

    public function removeReservationHistorique(ReservationHistorique $reservationHistorique): self
    {
        if ($this->reservationHistoriques->removeElement($reservationHistorique)) {
            // set the owning side to null (unless already changed)
            if ($reservationHistorique->getStatut() === $this) {
                $reservationHistorique->setStatut(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getNom();
    }
}
