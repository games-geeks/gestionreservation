<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CoachRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;


#[ORM\Entity(repositoryClass: CoachRepository::class)]
class Coach extends User
{

    #[ORM\Column(length: 255)]
    private ?string $surnom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $couleur = null;

    #[ORM\OneToMany(mappedBy: 'coach', targetEntity: Reservation::class)]
    private Collection $reservations;

    #[ORM\OneToMany(mappedBy: 'coach', targetEntity: ModeleReservation::class)]
    private Collection $modeleReservations;

    public function __construct()
    {

        $this->modeleReservations = new ArrayCollection();
        $this->reservations = new ArrayCollection();
    }


    public function getSurnom(): ?string
    {
        return $this->surnom;
    }

    public function setSurnom(string $surnom): static
    {
        $this->surnom = $surnom;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(?string $couleur): static
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * @return Collection<int, ModeleReservation>
     */
    public function getModeleReservations(): Collection
    {
        return $this->modeleReservations;
    }

    public function addModeleReservation(ModeleReservation $modeleReservation): static
    {
        if (!$this->modeleReservations->contains($modeleReservation)) {
            $this->modeleReservations->add($modeleReservation);
            $modeleReservation->setCoach($this);
        }

        return $this;
    }

    public function removeModeleReservation(ModeleReservation $modeleReservation): static
    {
        if ($this->modeleReservations->removeElement($modeleReservation)) {
            // set the owning side to null (unless already changed)
            if ($modeleReservation->getCoach() === $this) {
                $modeleReservation->setCoach(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reservation $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations->add($reservation);
            $reservation->setCoach($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getCoach() === $this) {
                $reservation->setCoach(null);
            }
        }

        return $this;
    }
}
