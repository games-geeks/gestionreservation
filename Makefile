SHELL := /bin/bash

cities:
	curl -L -o import/cities.csv https://www.data.gouv.fr/fr/datasets/r/51606633-fb13-4820-b795-9a2a575a72f1
	head -1 import/cities.csv > import/cities_u.csv
	grep -v insee import/cities.csv >import/corps
	cat import/corps |sort -u >> import/cities_u.csv
	wc -l import/cities_u.csv
	rm import/corps
	php bin/console app:import-cities
.PHONY: cities
abonnements:
	php bin/console app:import-abonnements
.PHONY: abonnements