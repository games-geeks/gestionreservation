composer require symfony/webpack-encore-bundle
composer require symfony/ux-autocomplete
composer require league/csv
composer require --dev orm-fixtures
composer require --dev fakerphp/faker
npm install --force && npm run build
composer require easycorp/easyadmin-bundle
symfony console make:admin:dashboard
docker compose up -d
symfony serve -d

<!-- composer.bat req symfony/twig-pack -->