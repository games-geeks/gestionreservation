<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240410180624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abonnements_type (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, tarif INT NOT NULL, nombre_lecon INT NOT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE achats (id INT AUTO_INCREMENT NOT NULL, joueur_id INT DEFAULT NULL, abonnement_id INT DEFAULT NULL, prix_paye INT NOT NULL, remise INT DEFAULT NULL, prix_abonnement INT NOT NULL, status LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_9920924EA9E2D76C (joueur_id), INDEX IDX_9920924EF1D74413 (abonnement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE centres (id INT AUTO_INCREMENT NOT NULL, localite_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, adresse_complementaire VARCHAR(255) DEFAULT NULL, mail VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, twitter VARCHAR(255) DEFAULT NULL, facebook VARCHAR(255) DEFAULT NULL, instagram VARCHAR(255) DEFAULT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_3BA7EA52924DD2B5 (localite_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, insee_code VARCHAR(255) NOT NULL, city_code VARCHAR(255) NOT NULL, zip_code VARCHAR(255) NOT NULL, label LONGTEXT NOT NULL, latitude VARCHAR(255) NOT NULL, longitude VARCHAR(255) NOT NULL, department_name VARCHAR(255) NOT NULL, department_number VARCHAR(255) NOT NULL, region_name VARCHAR(255) NOT NULL, region_geo_json_name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE coach (id INT NOT NULL, surnom VARCHAR(255) NOT NULL, couleur VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modele_reservation (id INT AUTO_INCREMENT NOT NULL, coach_id INT NOT NULL, jour INT NOT NULL, heure_debut TIME NOT NULL, heure_fin TIME NOT NULL, nombre_max INT DEFAULT NULL, is_active TINYINT(1) NOT NULL, INDEX IDX_8881DCC83C105691 (coach_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE modele_reservation_niveaux (modele_reservation_id INT NOT NULL, niveaux_id INT NOT NULL, INDEX IDX_B3E599404ECF3BE5 (modele_reservation_id), INDEX IDX_B3E59940AAC4B70E (niveaux_id), PRIMARY KEY(modele_reservation_id, niveaux_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE niveaux (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, level INT DEFAULT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation (id INT AUTO_INCREMENT NOT NULL, coach_id INT DEFAULT NULL, modele_id INT DEFAULT NULL, jour DATETIME NOT NULL, heure_debut TIME NOT NULL, heure_fin TIME NOT NULL, nombre_max INT DEFAULT 6 NOT NULL, nombre_encours INT DEFAULT 0, semaine INT DEFAULT NULL, is_active TINYINT(1) DEFAULT NULL, INDEX IDX_42C849553C105691 (coach_id), INDEX IDX_42C84955AC14B70A (modele_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation_niveaux (reservation_id INT NOT NULL, niveaux_id INT NOT NULL, INDEX IDX_3162CA25B83297E7 (reservation_id), INDEX IDX_3162CA25AAC4B70E (niveaux_id), PRIMARY KEY(reservation_id, niveaux_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation_user (reservation_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_9BAA1B21B83297E7 (reservation_id), INDEX IDX_9BAA1B21A76ED395 (user_id), PRIMARY KEY(reservation_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservation_historique (reservation_id INT NOT NULL, joueur_id INT NOT NULL, origine_id INT DEFAULT NULL, statut_id INT DEFAULT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_8D151C58B83297E7 (reservation_id), INDEX IDX_8D151C58A9E2D76C (joueur_id), INDEX IDX_8D151C5887998E (origine_id), INDEX IDX_8D151C58F6203804 (statut_id), PRIMARY KEY(reservation_id, joueur_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE statut (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, couleur VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, telephone VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, reset_token VARCHAR(100) DEFAULT NULL, creeatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', updatedAt DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', discr VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_IDENTIFIER_EMAIL (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', available_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', delivered_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE achats ADD CONSTRAINT FK_9920924EA9E2D76C FOREIGN KEY (joueur_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE achats ADD CONSTRAINT FK_9920924EF1D74413 FOREIGN KEY (abonnement_id) REFERENCES abonnements_type (id)');
        $this->addSql('ALTER TABLE centres ADD CONSTRAINT FK_3BA7EA52924DD2B5 FOREIGN KEY (localite_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE coach ADD CONSTRAINT FK_3F596DCCBF396750 FOREIGN KEY (id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE modele_reservation ADD CONSTRAINT FK_8881DCC83C105691 FOREIGN KEY (coach_id) REFERENCES coach (id)');
        $this->addSql('ALTER TABLE modele_reservation_niveaux ADD CONSTRAINT FK_B3E599404ECF3BE5 FOREIGN KEY (modele_reservation_id) REFERENCES modele_reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE modele_reservation_niveaux ADD CONSTRAINT FK_B3E59940AAC4B70E FOREIGN KEY (niveaux_id) REFERENCES niveaux (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849553C105691 FOREIGN KEY (coach_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955AC14B70A FOREIGN KEY (modele_id) REFERENCES modele_reservation (id)');
        $this->addSql('ALTER TABLE reservation_niveaux ADD CONSTRAINT FK_3162CA25B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation_niveaux ADD CONSTRAINT FK_3162CA25AAC4B70E FOREIGN KEY (niveaux_id) REFERENCES niveaux (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation_user ADD CONSTRAINT FK_9BAA1B21B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation_user ADD CONSTRAINT FK_9BAA1B21A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reservation_historique ADD CONSTRAINT FK_8D151C58B83297E7 FOREIGN KEY (reservation_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE reservation_historique ADD CONSTRAINT FK_8D151C58A9E2D76C FOREIGN KEY (joueur_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE reservation_historique ADD CONSTRAINT FK_8D151C5887998E FOREIGN KEY (origine_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE reservation_historique ADD CONSTRAINT FK_8D151C58F6203804 FOREIGN KEY (statut_id) REFERENCES statut (id)');
        $this->addSql('ALTER TABLE joueur ADD CONSTRAINT FK_FD71A9C5B3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveaux (id)');
        $this->addSql('ALTER TABLE joueur ADD CONSTRAINT FK_FD71A9C5BF396750 FOREIGN KEY (id) REFERENCES `user` (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE joueur DROP FOREIGN KEY FK_FD71A9C5B3E9C81');
        $this->addSql('ALTER TABLE joueur DROP FOREIGN KEY FK_FD71A9C5BF396750');
        $this->addSql('ALTER TABLE achats DROP FOREIGN KEY FK_9920924EA9E2D76C');
        $this->addSql('ALTER TABLE achats DROP FOREIGN KEY FK_9920924EF1D74413');
        $this->addSql('ALTER TABLE centres DROP FOREIGN KEY FK_3BA7EA52924DD2B5');
        $this->addSql('ALTER TABLE coach DROP FOREIGN KEY FK_3F596DCCBF396750');
        $this->addSql('ALTER TABLE modele_reservation DROP FOREIGN KEY FK_8881DCC83C105691');
        $this->addSql('ALTER TABLE modele_reservation_niveaux DROP FOREIGN KEY FK_B3E599404ECF3BE5');
        $this->addSql('ALTER TABLE modele_reservation_niveaux DROP FOREIGN KEY FK_B3E59940AAC4B70E');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849553C105691');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955AC14B70A');
        $this->addSql('ALTER TABLE reservation_niveaux DROP FOREIGN KEY FK_3162CA25B83297E7');
        $this->addSql('ALTER TABLE reservation_niveaux DROP FOREIGN KEY FK_3162CA25AAC4B70E');
        $this->addSql('ALTER TABLE reservation_user DROP FOREIGN KEY FK_9BAA1B21B83297E7');
        $this->addSql('ALTER TABLE reservation_user DROP FOREIGN KEY FK_9BAA1B21A76ED395');
        $this->addSql('ALTER TABLE reservation_historique DROP FOREIGN KEY FK_8D151C58B83297E7');
        $this->addSql('ALTER TABLE reservation_historique DROP FOREIGN KEY FK_8D151C58A9E2D76C');
        $this->addSql('ALTER TABLE reservation_historique DROP FOREIGN KEY FK_8D151C5887998E');
        $this->addSql('ALTER TABLE reservation_historique DROP FOREIGN KEY FK_8D151C58F6203804');
        $this->addSql('DROP TABLE abonnements_type');
        $this->addSql('DROP TABLE achats');
        $this->addSql('DROP TABLE centres');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE coach');
        $this->addSql('DROP TABLE modele_reservation');
        $this->addSql('DROP TABLE modele_reservation_niveaux');
        $this->addSql('DROP TABLE niveaux');
        $this->addSql('DROP TABLE reservation');
        $this->addSql('DROP TABLE reservation_niveaux');
        $this->addSql('DROP TABLE reservation_user');
        $this->addSql('DROP TABLE reservation_historique');
        $this->addSql('DROP TABLE statut');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
